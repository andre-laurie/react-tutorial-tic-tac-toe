// Licensed under the MIT license. See LICENSE file in the project root for details.
import ReactDOM from 'react-dom/client';
import './App.css';

import Game from './App';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(<Game />);

// Licensed under the MIT license. See LICENSE file in the project root for details.
import React, { useCallback, useContext, useReducer } from 'react';

const Dispatch = React.createContext(null);

function Square(props) {
  const { i } = props;
  const dispatch = useContext(Dispatch);
  const memoizedOnClick = useCallback(
    () => dispatch({ type: 'handleClick', i }),
    [i, dispatch]
  );
  return (
    <button
      className="square"
      onClick={memoizedOnClick}
      data-testid={props.dataTestId}
      children={props.value}
    />
  );
}

function Board(props) {
  function renderSquare(i) {
    return <Square dataTestId={'square' + i} value={props.squares[i]} i={i} />;
  }
  return (
    <div>
      <div className="board-row">
        {renderSquare(0)}
        {renderSquare(1)}
        {renderSquare(2)}
      </div>
      <div className="board-row">
        {renderSquare(3)}
        {renderSquare(4)}
        {renderSquare(5)}
      </div>
      <div className="board-row">
        {renderSquare(6)}
        {renderSquare(7)}
        {renderSquare(8)}
      </div>
    </div>
  );
}

function Move(props) {
  const { index } = props;
  const dispatch = useContext(Dispatch);
  const description = index ? `Cancel step ${index + 1}` : 'Reset';
  const memoizedJumpTo = useCallback(
    () => dispatch({ type: 'jumpTo', i: index }),
    [index, dispatch]
  );
  return (
    <li>
      <button onClick={memoizedJumpTo}>{description}</button>
    </li>
  );
}

export default function Game() {
  function reducer(history, action) {
    switch (action.type) {
      case 'jumpTo':
        return jumpTo();
      case 'handleClick':
        return handleClick();
      default:
        throw new Error('Unexpected action');
    }

    function jumpTo() {
      const i = action.i;
      return history.slice(0, i + 1);
    }

    function handleClick() {
      const i = action.i;
      const [lastHistoryItem] = history.slice(-1);
      const { squares, isXNext } = lastHistoryItem;

      if (squares[i] || calculateWinner(squares)) return history;
      const newSquares = squares.slice();
      newSquares[i] = isXNext ? 'X' : 'O';
      const newHistoryItem = {
        squares: newSquares,
        isXNext: !isXNext,
      };
      return [...history, newHistoryItem];
    }
  }

  const [history, dispatch] = useReducer(reducer, [
    {
      squares: Array(9).fill(null),
      isXNext: true,
    },
  ]);

  const [lastHistoryItem] = history.slice(-1);
  const { squares, isXNext } = lastHistoryItem;
  const winner = calculateWinner(squares);
  const status = winner
    ? `Winner: ${winner}`
    : `Next player: ${isXNext ? 'X' : 'O'}`;

  const moves = history
    .slice(0, -1)
    .map((_, index) => <Move key={index} index={index} />);

  return (
    <Dispatch.Provider value={dispatch}>
      <div className="game">
        <div className="game-board">
          <Board squares={squares} isXNext={isXNext} />
        </div>
        <div className="game-info">
          <div data-testid="game-status">{status}</div>
          <ol data-testid="game-moves">{moves}</ol>
        </div>
      </div>
    </Dispatch.Provider>
  );
}

// ========================================
function calculateWinner(squares) {
  const lines = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6],
  ];
  for (let i = 0; i < lines.length; i++) {
    const [a, b, c] = lines[i];
    if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
      return squares[a];
    }
  }
  return null;
}

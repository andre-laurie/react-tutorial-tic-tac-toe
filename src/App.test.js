// Licensed under the MIT license. See LICENSE file in the project root for details.
import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';

import Game from './App';

test('Initial display', () => {
	render(<Game />);

	expect(screen.getByTestId('game-status').textContent).toBe('Next player: X');
	expect(screen.getByTestId('game-moves')).toBeEmptyDOMElement();
	for (let i = 0; i < 9; i++) {
		const square = screen.getByTestId('square' + i);
		expect(square).toBeInTheDocument();
		expect(square).toBeEmptyDOMElement();
	}
});

test('Alternate turns', () => {
	render(<Game />);

	expect(screen.getByTestId('game-status').textContent).toBe('Next player: X');
	const square0 = screen.getByTestId('square0');
	fireEvent.click(square0);
	expect(square0).toHaveTextContent('X');

	expect(screen.getByTestId('game-status').textContent).toBe('Next player: O');

	const square1 = screen.getByTestId('square1');
	fireEvent.click(square1);
	expect(square1).toHaveTextContent('O');
	expect(screen.getByTestId('game-status').textContent).toBe('Next player: X');

	const square2 = screen.getByTestId('square2');
	fireEvent.click(square2);
	expect(square2).toHaveTextContent('X');
});

test('No click on already filled square', () => {
	render(<Game />);

	const square0 = screen.getByTestId('square0');
	fireEvent.click(square0);
	fireEvent.click(square0);
	expect(square0).toHaveTextContent('X');

	expect(screen.getByTestId('game-status').textContent).toBe('Next player: O');
});

test('No click when game already won', () => {
	render(<Game />);

	fireEvent.click(screen.getByTestId('square0')); //X
	fireEvent.click(screen.getByTestId('square3')); //O
	fireEvent.click(screen.getByTestId('square1')); //X
	fireEvent.click(screen.getByTestId('square4')); //O
	fireEvent.click(screen.getByTestId('square2')); //X

	const square5 = screen.getByTestId('square5');
	fireEvent.click(square5); //O
	expect(square5).toBeEmptyDOMElement();
});

test('X wins', () => {
	render(<Game />);

	/*
	 * XXX
	 * OO.
	 * ...
	 */

	fireEvent.click(screen.getByTestId('square0')); //X
	fireEvent.click(screen.getByTestId('square3')); //O
	fireEvent.click(screen.getByTestId('square1')); //X
	fireEvent.click(screen.getByTestId('square4')); //O
	fireEvent.click(screen.getByTestId('square2')); //X

	expect(screen.getByTestId('game-status').textContent).toBe('Winner: X');
});

test('O wins', () => {
	render(<Game />);

	/*
	 * XX.
	 * OOO
	 * X..
	 */

	fireEvent.click(screen.getByTestId('square0')); //X
	fireEvent.click(screen.getByTestId('square3')); //O
	fireEvent.click(screen.getByTestId('square1')); //X
	fireEvent.click(screen.getByTestId('square4')); //O
	fireEvent.click(screen.getByTestId('square6')); //X
	fireEvent.click(screen.getByTestId('square5')); //O

	expect(screen.getByTestId('game-status').textContent).toBe('Winner: O');
});

test('History display', () => {
	render(<Game />);

	fireEvent.click(screen.getByTestId('square0'));
	expect(screen.getByText('Reset')).toBeInTheDocument();
	fireEvent.click(screen.getByTestId('square1'));
	expect(screen.getByText('Cancel step 2')).toBeInTheDocument();
	fireEvent.click(screen.getByTestId('square2'));
	expect(screen.getByText('Cancel step 3')).toBeInTheDocument();
});

test('History cancel single level', () => {
	render(<Game />);

	const square0 = screen.getByTestId('square0');
	fireEvent.click(square0); // 1. X
	const square1 = screen.getByTestId('square1'); // 2. O
	fireEvent.click(square1);
	const square2 = screen.getByTestId('square2'); //3. X
	fireEvent.click(square2);
	fireEvent.click(screen.getByText('Cancel step 3'));

	expect(square0).toHaveTextContent('X');
	expect(square1).toHaveTextContent('O');
	expect(square2).toBeEmptyDOMElement();

	expect(screen.getByTestId('game-status').textContent).toBe('Next player: X');
});

test('History cancel multiple levels', () => {
	render(<Game />);

	const square0 = screen.getByTestId('square0');
	fireEvent.click(square0); // 1. X
	const square1 = screen.getByTestId('square1'); // 2. O
	fireEvent.click(square1);
	const square2 = screen.getByTestId('square2'); //3. X
	fireEvent.click(square2);
	fireEvent.click(screen.getByText('Cancel step 2'));

	expect(square0).toHaveTextContent('X');
	expect(square1).toBeEmptyDOMElement();
	expect(square2).toBeEmptyDOMElement();

	expect(screen.getByTestId('game-status').textContent).toBe('Next player: O');
});

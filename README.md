# ReactJS tutorial: Tic Tac Toe

Inspired from [intro tutorial](https://fr.reactjs.org/tutorial/tutorial.html).

## Setup differences

eslint-config, eslint-plugin, prettier

## Implementation differences

In the tutorial `handleClick` is defined in the root component: Game and has two responsabilites:

- creating next board state (squares and player's turn)
- handling history

=> I split it up:

- `push` is defined in Game and handles history
- `handleClick` is defined in Board and handles Board next state, then uses `push` delegate from Game.

By the way, I implemented `undo` instead of `review turn`, but who cares…

## Tests added

Using:

- jest, jest-dom
- testing-library/react
